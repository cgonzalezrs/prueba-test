Feature: Look at the overall display of the footer

  Scenario: Searching for a term
    Given Sergey is on the Sura home page
    When he searches for "auto"
    Then all the result titles should contain the word "auto"

  Scenario Outline: Footer social links are working
    Given Sergey is on the Sura home page
    When he scroll down to the footer
    Then he click on the social media icon "<social>"
    And he is being redirect to the facebook website "<link>"

    Examples:
      | social    | link                                                                               |
      | facebook  | https://www.facebook.com/SURAPanama                                                |
      | twitter   | https://twitter.com/SegurosSURAPma?lang=es                                         |
      | instagram | https://www.instagram.com/segurossurapanama/                                       |
      | youtube   | https://www.youtube.com/channel/UCqsCdxc9XQdthUHooU_lgYg/videos?view_as=subscriber |