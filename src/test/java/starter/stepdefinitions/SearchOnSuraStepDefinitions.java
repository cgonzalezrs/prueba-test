package starter.stepdefinitions;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.cucumber.java.en.And;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.serenitybdd.screenplay.ensure.Ensure;
import net.serenitybdd.screenplay.waits.WaitUntil;
import starter.navigation.NavigateTo;
import starter.navigation.ScrollTo;
import starter.search.SearchFor;
import starter.search.SearchResult;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.*;
import static org.hamcrest.Matchers.*;
import static starter.matchers.StringContainsIgnoringCase.containsIgnoringCase;

public class SearchOnSuraStepDefinitions {

    @Before
    public void setTheStage() {
        OnStage.setTheStage(new OnlineCast());
    }

    @Given("^(.*) is on the Sura home page")
    public void on_the_Sura_home_page(String actor) {
        theActorCalled(actor).attemptsTo(NavigateTo.theSuraHomePage());
    }

    @When("she/he searches for {string}")
    public void search_for(String term) {
        withCurrentActor(
                SearchFor.term(term)
        );
    }

    @Then("all the result titles should contain the word {string}")
    public void all_the_result_titles_should_contain_the_word(String term) {
        withCurrentActor(
                Ensure.thatTheAnswersTo(SearchResult.titles())
                        .allMatch("a title containing '" + term + "'",
                                title -> title.toLowerCase().contains(term.toLowerCase()))

        );

        theActorInTheSpotlight().should(
                seeThat("search result titles",
                        SearchResult.titles(), hasSize(greaterThan(0))),
                seeThat("search result titles",
                        SearchResult.titles(), everyItem(containsIgnoringCase(term)))
        );
    }

    @Then("she/he scroll down to the footer")
    public void scroll_down_to_the_footer() {
        withCurrentActor(
                ScrollTo.theSuraFooterSection()
        );


    }

    @Then("she/he click on the social media icon {string}")
    public void footer_links(String social){
        if (social.equals("facebook")) {
            withCurrentActor(
                    SearchFor.btn_fb()
            );
        }

        if (social.equals("twitter")) {
            withCurrentActor(
                    SearchFor.btn_twitter()
            );
        }

        if (social.equals("instagram")) {
            withCurrentActor(
                    SearchFor.btn_instagram()
            );
        }

        if (social.equals("youtube")) {
            withCurrentActor(
                    SearchFor.btn_youtube()
            );
        }
}

    @And("she/he is being redirect to the facebook website {string}")
    public void heIsBeingRedirectToTheFacebookWebsiteLink(String arg0) {
        withCurrentActor(
                SearchFor.getToOtherWindow(),
                Ensure.thatTheCurrentPage().currentUrl().isEqualTo(arg0)
        );
    }
}
