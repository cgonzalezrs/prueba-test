package starter.navigation;

import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Open;

public class NavigateTo {

    public static Performable theSuraHomePage() {
        return Task.where("{0} opens the Sura home page",
                Open.browserOn().the(SuraHomePage.class)
        );
    }
}