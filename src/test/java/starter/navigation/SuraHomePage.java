package starter.navigation;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://segurossura.com.pa/")
public class SuraHomePage extends PageObject {}
