package starter.navigation;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Scroll;
import starter.search.SearchForm;

public class ScrollTo {


    public static Performable theSuraFooterSection() {
        return Task.where("{0} attempts to scroll down to the footer",
                Scroll.to(SearchForm.FOOTER_SECTION)
        );
    }
}
