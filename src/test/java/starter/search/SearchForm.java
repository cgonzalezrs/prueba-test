package starter.search;

import org.openqa.selenium.By;

public class SearchForm {
    public static By SEARCH_FIELD = By.cssSelector("input[class='input-group-field search-field']");
    public static By SEARCH_BUTTON = By.cssSelector("div.input-group-button button.button");
    public static By FOOTER_SECTION = By.id("footer");
    public static By BTN_FACEBOOK = By.cssSelector("#footer-container #footer .social-networks .menu-item a svg");
    public static By BTN_TWITTER = By.cssSelector("#footer-container #footer .social-networks .menu-item:nth-child(2) a svg");
    public static By BTN_INSTAGRAM = By.cssSelector("#footer-container #footer .social-networks .menu-item:nth-child(3) a svg");
    public static By BTN_YOUTUBE = By.cssSelector("#footer-container #footer .social-networks .menu-item:nth-child(4) a svg");
}
