package starter.search;

import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Clear;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.DriverTask;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.waits.WaitUntil;
import net.serenitybdd.screenplay.waits.WaitUntilTargetIsReady;

public class SearchFor {

    public static Performable term(String term) {
        return Task.where("{0} attempts to search for #term",
                Clear.field(SearchForm.SEARCH_FIELD),
                Enter.theValue(term).into(SearchForm.SEARCH_FIELD),
                Click.on(SearchForm.SEARCH_BUTTON)
        ).with("term").of(term);
    }

    public static Performable btn_fb() {
        return Task.where("{0} attempts to click in fb button",
                Click.on(SearchForm.BTN_FACEBOOK)
                );
    }

    public static Performable btn_twitter() {
        return Task.where("{0} attempts to click in fb button",
                Click.on(SearchForm.BTN_TWITTER)
        );
    }

    public static Performable btn_instagram() {
        return Task.where("{0} attempts to click in fb button",
                Click.on(SearchForm.BTN_INSTAGRAM)
        );
    }

    public static Performable btn_youtube() {
        return Task.where("{0} attempts to click in fb button",
                Click.on(SearchForm.BTN_YOUTUBE)
        );
    }

    public static Performable getToOtherWindow() {
        return new DriverTask(driver -> {
            String currentWindow = driver.getWindowHandle();
            driver.getWindowHandles().stream()
                    .filter(handle -> !handle.equals(currentWindow))
                    .findFirst()
                    .ifPresent(
                            otherWindowHandle -> driver.switchTo().window(otherWindowHandle)
                    );
        }
        );
    }


}
